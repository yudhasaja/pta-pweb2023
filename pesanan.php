<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $nama = $_POST['name'];
    $barang = $_POST['product'];
    $quantity = $_POST['quantity'];

    $pesanan = 
    "Nama:$nama|Barang:$barang|Jumlah:$quantity" . PHP_EOL;

    file_put_contents('pesanan.txt', $pesanan, FILE_APPEND);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Terima Kasih</title>
</head>
<body>
    <h1>Terima Kasih!</h1>
    <p>Pesanan Anda telah diterima.</p>
    <p>Detail Pesanan:</p>
    <ul>
        <li>Nama: <?php echo $nama; ?></li>
        <li>Barang: <?php echo $barang; ?></li>
        <li>Jumlah: <?php echo $quantity; ?></li>
    </ul>
    <a href="index.html">Kembali ke Halaman Utama</a>
</body>
</html>
<?php
}
?>
